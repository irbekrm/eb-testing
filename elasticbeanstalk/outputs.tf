output "cname" {
    description = "DNS name for the environment"
    value = "${aws_elastic_beanstalk_environment.live.cname}"
}