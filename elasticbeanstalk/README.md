## Use
1. Run terraform scripts in `/infra` to create VPC, subnet and AWS Keypair
2. Run terraform scripts in `/iam` to create Service Role and IAM Profile for ElasticBeanstalk
3. Use terraform module `/elasticbeanstalk` as a child module for the terraform scripts in `tda.devops.terraform.<tda-service>` passing the input vars specified below
(See example in `/elasticbeanstalk/examples`)

## Creates
An ElasticBeanstalk application, a single instance environment with an Autoscaling group with one EC2 instance (default instance type is t3.micro).

## Required Inputs

The following input variables are required:

### bucket\_name

Description: S3 bucket with the application source bundle

Type: `string`

### local\_app\_source\_bundle\_path

Description: The relative path of the application source bundle

Type: `string`

### service

Description: TDA service

Type: `string`

### ssh\_source\_restriction

Description: The CIDR range that will be given ssh access to the autoscaling group

Type: `string`

## Optional Inputs

The following input variables are optional (have default values):

### additional\_security\_group

Description: An additional security group for Autoscaling group. ElasticBeanstalk will create a default one that whitelists all IPs for port 80 and source IP for port 22

Type: `string`

Default: `""`

### environment

Description: Name of AWS ElasticBeanstalk to be created

Type: `string`

Default: `"default"`

### instance\_type

Description: Type of EC2 instances

Type: `string`

Default: `"t3.micro"`

### region

Description:

Type: `string`

Default: `"eu-central-1"`

### solution\_stack\_name

Description: AWS solution stack for the desired platform type

Type: `string`

Default: `"64bit Amazon Linux 2018.03 v2.12.10 running Docker 18.06.1-ce"`

### version

Description: Name of the application version to be deployed

Type: `string`

Default: `"default"`

## Outputs

The following outputs are exported:

### cname

Description: DNS name for the environment


