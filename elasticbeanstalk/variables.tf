variable "region" {
    default = "eu-central-1"
}
variable "version" {
    description = "Name of the application version to be deployed"
    default = "default"
}
variable "environment" {
    description = "Name of AWS ElasticBeanstalk to be created"
    default = "live"
}
variable "solution_stack_name" {
    description = "AWS solution stack for the desired platform type"
    default = "64bit Amazon Linux 2018.03 v2.12.10 running Docker 18.06.1-ce"
}
variable "instance_type" {
    description = "Type of EC2 instances"
    default = "t3.micro"
}

variable "additional_security_group" {
    default = ""
    description = "An additional security group for Autoscaling group. ElasticBeanstalk will create a default one that whitelists all IPs for port 80 and source IP for port 22"
}


variable "service" {
    description = "TDA service"
}
variable "bucket_name" {
    description = "S3 bucket with the application source bundle"
}

variable "ssh_source_restriction" {
    description = "The CIDR range that will be given ssh access to the autoscaling group"
  
}

variable "local_app_source_bundle_path" {
    description = "The relative path of the application source bundle"
}

variable "zone_id" {
    description = "ID of the hosted zone where the route 53 record for the new domain name should be created"
}
