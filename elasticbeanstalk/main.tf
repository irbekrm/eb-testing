provider "aws" {
    region = "${var.region}"
}

data "terraform_remote_state" "infra" {
  backend = "s3"
  config {
    bucket = "eb-test9106"
    key = "tfstates/infra/terraform.tfstate"
    region = "${var.region}"
  }
}

data "terraform_remote_state" "iam" {
  backend = "s3"
  config {
    bucket = "eb-test9106"
    key = "tfstates/iam/terraform.tfstate"
    region = "${var.region}"
  }
}
data "aws_s3_bucket" "app_source_bundle" {
    bucket = "${var.bucket_name}"
}

resource "aws_s3_bucket_object" "app_source_bundle_path" {
    bucket = "${var.bucket_name}"
    key = "app-source-bundles/${var.service}/${var.version}.zip"
    source = "${var.local_app_source_bundle_path}"
}

resource "aws_elastic_beanstalk_application" "default" {
    name = "${var.service}"
    description = "eb application"
}

resource "aws_elastic_beanstalk_environment" "live" {
    depends_on = ["aws_elastic_beanstalk_application.default"]
    name = "${var.service}-${var.environment}"
    application = "${aws_elastic_beanstalk_application.default.name}"
    solution_stack_name = "${var.solution_stack_name}"
    version_label = "${aws_elastic_beanstalk_application_version.default.name}"
  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "ServiceRole"
    value     = "${data.terraform_remote_state.iam.service_role_name}"
    }
  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "EnvironmentType"
    value     = "SingleInstance"
}

  setting {
      namespace = "aws:autoscaling:launchconfiguration"
      name      = "IamInstanceProfile"
      value     = "${data.terraform_remote_state.iam.instance_profile_name}"
  }
  setting {
      namespace = "aws:autoscaling:launchconfiguration"
      name      = "InstanceType"
      value     = "${var.instance_type}"
  }

  setting {
      namespace = "aws:autoscaling:launchconfiguration"
      name = "EC2KeyName"
      value = "${data.terraform_remote_state.infra.ec2_key_pair_name}"
  }
  
  setting {
      namespace = "aws:autoscaling:launchconfiguration"
      name = "SecurityGroups"
      value = "${var.additional_security_group}"
  }
  
  setting {
      namespace = "aws:autoscaling:launchconfiguration"
      name      = "SSHSourceRestriction"
      value     = "tcp,22,22,${var.ssh_source_restriction}"
  }

  setting {
      namespace = "aws:ec2:vpc"
      name = "VPCId"
      value = "${data.terraform_remote_state.infra.vpc_id}"
  }
  setting {
      namespace = "aws:ec2:vpc"
      name = "Subnets"
      value = "${data.terraform_remote_state.infra.public_subnet_id}"
  }
  setting {
      namespace = "aws:autoscaling:asg"
      name      = "MaxSize"
      value     = 1
  }

}

resource "aws_elastic_beanstalk_application_version" "default" {
    depends_on = ["aws_elastic_beanstalk_application.default"]
    name = "default_version"
    application = "${var.service}"
    description = "${var.service} version created by Terraform"
    bucket = "${data.aws_s3_bucket.app_source_bundle.id}"
    key = "${aws_s3_bucket_object.app_source_bundle_path.id}"
    
}

data "aws_elastic_beanstalk_hosted_zone" "current" {}

resource "aws_route53_record" "default" {
  zone_id = "${var.zone_id}"
  name = "${var.service}"
  type = "A"

  alias {
    name = "${aws_elastic_beanstalk_environment.live.cname}"
    zone_id = "${data.aws_elastic_beanstalk_hosted_zone.current.id}"
    evaluate_target_health = true
  }
}