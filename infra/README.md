## Required Inputs

The following input variables are required:

## Optional Inputs

The following input variables are optional (have default values):

### project

Description: Main project name

Type: `string`

Default: `"tda-elasticbeanstalk"`

### region

Description:

Type: `string`

Default: `"eu-central-1"`

## Outputs

The following outputs are exported:

### ec2\_key\_pair\_name

Description: AWS key pair to be used by ELB on the EC2 instances

### public\_subnet\_id

Description: The public subnet inside which TDA services will be placed

### ssh\_private\_key

Description: Private key for ssh access to the EC2 instances running TDA services

### vpc\_id

Description: The main VPC inside which TDA services will be placed

