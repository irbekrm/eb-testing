variable "project" {
    description = "Main project name"
    default = "tda-elasticbeanstalk"
}

variable "region" {
    default = "eu-central-1"
}