terraform {
    backend "s3" {
        bucket = "eb-test9106"
        key = "tfstates/infra/terraform.tfstate"
        region = "eu-central-1"
    }
}

provider "aws" {
    region = "${var.region}"
}

resource "tls_private_key" "default" {
    algorithm = "RSA"
}

resource "aws_key_pair" "tda_eb_default" {
    key_name = "${var.project}"
    public_key = "${tls_private_key.default.public_key_openssh}"
}


resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
  tags = {
      Name = "${var.project}_default"
  }
}

resource "aws_internet_gateway" "default" {
    vpc_id = "${aws_vpc.main.id}"
    tags = {
        Name = "${var.project}"
    }
}

resource "aws_route_table_association" "default" {
    subnet_id = "${aws_subnet.public.id}"
    route_table_id = "${aws_route_table.default.id}"
}

resource "aws_route_table" "default" {
    vpc_id = "${aws_vpc.main.id}"
    
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.default.id}"
    }
}

resource "aws_subnet" "public" {
    vpc_id = "${aws_vpc.main.id}"
    cidr_block = "10.0.0.0/24"
    map_public_ip_on_launch = true

    tags = {
        Name = "${var.project}_public"
    }
}