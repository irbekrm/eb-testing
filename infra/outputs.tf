output "ssh_private_key" {
    description = "Private key for ssh access to the EC2 instances running TDA services"
    value = "${tls_private_key.default.private_key_pem}"
}

output "ec2_key_pair_name" {
    description = "AWS key pair to be used by ELB on the EC2 instances"
    value = "${aws_key_pair.tda_eb_default.key_name}"
}

output "vpc_id" {
    description = "The main VPC inside which TDA services will be placed"
    value = "${aws_vpc.main.id}"
}

output "public_subnet_id" {
    description = "The public subnet inside which TDA services will be placed"
    value = "${aws_subnet.public.id}"
}