terraform {
    backend "s3" {
        bucket = "tda.devops.terraform.elasticbeanstalk"
        key = "tfstates/server-1/terraform.tfstate"
        region = "eu-central-1"
    }
}

data "terraform_remote_state" "infra" {
  backend = "s3"
  config {
    bucket = "tda.devops.terraform.elasticbeanstalk"
    key = "tfstates/infra/terraform.tfstate"
    region = "eu-central-1"
  }
}

provider "aws" {
  region = "eu-central-1"
}


module "main" {
    source = "git::https://irbekrm@bitbucket.org/tuigroup/tda.devops.terraform.git//elasticbeanstalk/elasticbeanstalk?ref=Feature/LD-295-Create-Terraform-for-ElasticBeanstalk"
    service = "server-1"
    bucket_name = "tda.devops.terraform.elasticbeanstalk"
    environment = "default"
    ssh_source_restriction = "86.164.195.154/22"
    instance_type = "t2.micro"
    additional_security_group = "${aws_security_group.default.id}"
}

resource "aws_security_group" "default" {
    name = "tda-server-1"
    description = "Allow inbound traffic from provided CIDR blocks"
    vpc_id = "${data.terraform_remote_state.infra.vpc_id}"

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["6.166.195.154/32"]
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = ["0.0.0.0/0"]
    }
  
}