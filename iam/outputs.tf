output "instance_profile_name" {
    description = "Instance profile that will be assigned to the EC2 instances/Autoscaling group"
    value = "${aws_iam_instance_profile.eb_instance_profile.name}"
}

output "service_role_name" {
    description = "Service role for ElasticBeanstalk"
    value = "${aws_iam_role.beanstalk_service.name}"
}