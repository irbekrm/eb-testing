## Required Inputs

The following input variables are required:

## Optional Inputs

The following input variables are optional (have default values):

### region

Description:

Type: `string`

Default: `"eu-central-1"`

## Outputs

The following outputs are exported:

### instance\_profile\_name

Description: Instance profile that will be assigned to the EC2 instances/Autoscaling group

### service\_role\_name

Description: Service role for ElasticBeanstalk

